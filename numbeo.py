from selenium import webdriver
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup as Soup
import re
import os
import time
import json
import random

base_url = "https://www.numbeo.com/cost-of-living/"


def get_everything(driver, blacklist):
    driver.get(base_url)
    soup = Soup(driver.page_source, features="html.parser")
    select_country_element = soup.find('select', {'id': 'country'})
    country_options = [x.text for x in select_country_element.find_all('option')]
    countries = country_options[1:]
    data = {}
    for country in countries:
        if country in blacklist:
            continue
        try:
            attributes = []
            driver.get(base_url)
            select = Select(driver.find_element_by_xpath("//select[@id='country']"))
            select.select_by_visible_text(country)
            # select euro
            select = Select(driver.find_element_by_xpath("//select[@id='displayCurrency']"))
            select.select_by_visible_text("EUR")
            soup = Soup(driver.page_source, features="html.parser")
            tbody = soup.find('table', {'class': 'data_wide_table'}).tbody
            rows = tbody.find_all('tr')
            for r in rows:
                try:
                    if r.find('td', {'class': 'priceValue'}) is None:
                        continue
                    elements = rows[1].find_all('td')
                    name = elements[0].get_text()
                    value = float(re.findall("\d+\.\d+", elements[1].get_text())[0])
                    attributes.append({
                        'name': name,
                        'value': value
                    })
                except:
                    continue
            # time.sleep(random.uniform(1.0, 5.0))
            data[country] = {
                'attributes': attributes
            }
            # get general city data
            # ToDo
            # get data for each city
            country_url = driver.current_url
            select_city_element = soup.find('select', {'id': 'city'})
            city_options = [x.text for x in select_city_element.find_all('option')]
            cities = city_options[1:]
            for city in cities:
                try:
                    city_attributes = []
                    driver.get(country_url)
                    select = Select(driver.find_element_by_xpath("//select[@id='city']"))
                    select.select_by_visible_text(city)
                    # select euro
                    select = Select(driver.find_element_by_xpath("//select[@id='displayCurrency']"))
                    select.select_by_visible_text("EUR")
                    soup = Soup(driver.page_source, features="html.parser")
                    tbody = soup.find('table', {'class': 'data_wide_table'}).tbody
                    rows = tbody.find_all('tr')
                    for r in rows:
                        try:
                            if r.find('td', {'class': 'priceValue'}) is None:
                                continue
                            elements = rows[1].find_all('td')
                            name = elements[0].get_text()
                            value = float(re.findall("\d+\.\d+", elements[1].get_text())[0])
                            city_attributes.append({
                                'name': name,
                                'value': value
                            })
                        except:
                            continue
                    data[country][city] = {
                        'attributes': city_attributes
                    }
                    # time.sleep(random.uniform(1.0, 5.0))
                except:
                    continue
        except:
            continue
    return data


if __name__ == '__main__':
    url = "https://www.numbeo.com/cost-of-living/"

    driver = webdriver.Chrome()
    driver.implicitly_wait(30)
    driver.get(url)
    blacklist = []
    with open('data.json', 'r') as existing:
        existing_data = json.load(existing)
        blacklist = [c for c in existing_data]

    new_data = get_everything(driver, blacklist)
    data = {**existing_data, **new_data}
    with open('data2.json', 'w') as outfile:
        json.dump(data, outfile)

    time.sleep(2)
    driver.close()
